# Mimer flags

Verify flags and counting points

## Install

```
$ npm install
```

## Usage

```
npm run start
```

# Pages

## Open

### Index

### Login page

### flag-list

Listar alla flagor.

### flag-info

Give information aboput a flag.

## Need login

### flags-input

Where you writte in your flags

### flags-is-found

OBS!

## Need keywor or login

### List of result

### Garden

# Tabeller

### User

- _name_ A user naem
- _password_ A password.
- _permission_

## Tävlande

- keyword
- alias?
- userID
- points

## Flagor

- namn
- keyword
- points
- category

# Result

- crated Date
- tävlandeId
- flagID
- comments

# Tags

- namn

# TagLink

- flagsID
- tagsID

## License

GNU AFFERO GENERAL PUBLIC LICENSE Version 3, see the file COPYING.

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.0.1** _2022-12-05_ First version published

## Author

**©Magnus Kronnäs 2022 [magnus.kronnas.se](https://magnus.kronnas.se)**

# Default and flagor for mimer flags

** author: krm created: 2024-02-05 status: draft **

This is a collection of flags and badges for testing.

# 🗂️ First test category

** created: 2024-02-05 alias: This_is_a_start category: default point: 1 **

Easy to find.

If you reading this, you have find the key.

# 🎖️ First test badgaes

** created: 2024-02-05 alias: This_is_a_start category: default point: 1 **

Easy to find.

If you reading this, you have find the key.

# 🚩 First test flag

** created: 2024-02-05 alias: This_is_a_start category: default point: 1 **

Easy to find.

If you reading this, you have find the key.

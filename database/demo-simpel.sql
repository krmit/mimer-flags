USE flags;

/* Demo users */
INSERT INTO user VALUES (null, "oden", MD5("abc123"),"admin");
INSERT INTO user VALUES (null, "tor", MD5("troll"),"member");
INSERT INTO user VALUES (null, "frej", MD5("asd123"),"member");


/* Keywords */
INSERT INTO keyword VALUES (null, "abc123");
INSERT INTO keyword VALUES (null, "123");
INSERT INTO keyword VALUES (null, "abc");
INSERT INTO keyword VALUES (null, "123abc");

/* Values for demo flags */
INSERT INTO flag  VALUES (null, "start","Starta med ett test", "A first flag", "This_is_a_start", "{}", 1);
INSERT INTO flag  VALUES (null, "test","Ett till test", "A test flag", "This_is_a_test","{}", 1);
INSERT INTO flag  VALUES (null, "test-2","Ytterligare ett till test", "A test flag with 2 points", "This_is_a_test_2","{}", 2);
INSERT INTO flag  VALUES (null, "test-3","Test nummer tre eller fyra!", "A test flag with 3 points", "This_is_a_test_3","{}", 3);
INSERT INTO flag  VALUES (null, "byggare-Tom", "Byggare Tom!", "Byggar bygger muskler", "Is_big!","{}", 4);

/* Result */
INSERT INTO result VALUES (null, "2023-02-01 10:00:10",2,1);
INSERT INTO result VALUES (null, "2023-02-02 12:15:00",2,3);
INSERT INTO result VALUES (null, "2023-02-03 10:16:10",3,2);
INSERT INTO result VALUES (null, "2023-02-03 10:16:11",2,2);
INSERT INTO result VALUES (null, "2023-02-06 10:16:10",3,4);

DROP DATABASE flags;
CREATE DATABASE flags;
USE flags

/* Content */
CREATE TABLE user ( id INT NOT NULL AUTO_INCREMENT, alias CHAR(255), password CHAR(255), role CHAR(8), PRIMARY KEY (id));
CREATE TABLE keyword ( id INT NOT NULL AUTO_INCREMENT, alias CHAR(255), PRIMARY KEY (id));
CREATE TABLE flag ( id INT NOT NULL AUTO_INCREMENT, alias CHAR(255), title MEDIUMTEXT, description MEDIUMTEXT, word CHAR(255), reward TEXT, points INT, PRIMARY KEY (id));
CREATE TABLE badge ( id INT NOT NULL AUTO_INCREMENT, alias CHAR(255), title MEDIUMTEXT, description MEDIUMTEXT, reward TEXT, condition TEXT, PRIMARY KEY (id));
CREATE TABLE tag ( id INT NOT NULL AUTO_INCREMENT, alias CHAR(255), title MEDIUMTEXT, description MEDIUMTEXT, PRIMARY KEY (id));

/* Relations of type N:M */
CREATE TABLE result ( id INT NOT NULL AUTO_INCREMENT, created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,  user INT, flag INT, PRIMARY KEY (id));
CREATE TABLE merit ( id INT NOT NULL AUTO_INCREMENT,created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, user INT, badge INT, PRIMARY KEY (id));
CREATE TABLE link ( id INT NOT NULL AUTO_INCREMENT, flag INT, tag INT, PRIMARY KEY (id));
CREATE TABLE owner ( id INT NOT NULL AUTO_INCREMENT,created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, user INT, keyword INT, PRIMARY KEY (id));

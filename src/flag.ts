import { Request, Response } from "express";
import fs from "fs-extra";
import Mustache from "mustache";
import mysql from "mysql2/promise";
import { TokenData } from "./main";

export function showFlags(
  render: (path: string, data: any) => string,
  db: mysql.Connection
) {
  return async function (req: Request, res: Response) {
    db.execute("SELECT alias, title, points FROM flag;").then((flags) => {
      let data = {
        flags: flags[0],
      };
      let result = render("flags.html", data);
      res.send(result);
    });
  };
}

export function showFlag(
  render: (path: string, data: any) => string,
  db: mysql.Connection
) {
  return async function (req: Request, res: Response) {
    const alias = req.params["alias"];
    db.execute(
      `SELECT alias, title, points, description FROM flag WHERE alias="${alias}";`
    ).then((flags) => {
      const data = (flags as unknown as any)[0][0];
      data.symbol = "🏅".repeat(data.points);
      const result = render("flag.html", data);
      res.send(result);
    });
  };
}

export function confirmFlag(storage: TokenData, db: mysql.Connection) {
  return async function (req: Request, res: Response) {
    const flag = req.query["flag"];
    let flags_data = await db.execute(
      "SELECT * FROM flag WHERE alias='" + flag + "';"
    );
    let flag_data = flags_data.values().next().value;
    if (flag_data.length !== 0) {
      db.execute(
        `INSERT INTO result VALUES (null, null, ${res.locals.user.id}, ${flag_data[0].id});`
      );
      res.locals.user.points += flag_data[0].points;
      fs.readFile("template/flag-is-found.html", "utf-8").then((template) => {
        let result = Mustache.render(template, flag_data[0]);
        res.send(result);
      });
    } else {
      console.log("Ingen flaga!");
      res.send("Ingen flaga!");
    }
  };
}

import fs from "fs-extra";
import Mustache from "mustache";
import _path from "path";

export async function startRender(...pathList: string[]) {
  let templates: { [key: string]: string } = {};
  for (const path of pathList) {
    templates[path] = await fs.readFile(
      _path.join(__dirname, "..", "template", path),
      "utf-8"
    );
  }

  return function (path: string, data: any) {
    if (!templates.hasOwnProperty(path)) {
      throw "No templat file exsist: " + path;
    }
    return Mustache.render(templates[path], data);
  };
}

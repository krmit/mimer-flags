#!/usr/bin/node
import express from "express";
import { logger } from "./logger";
import { notFound, serverError } from "./errors";
import fs from "fs-extra";
import mysql from "mysql2/promise";
import cookieParser from "cookie-parser";
import { showFlags, showFlag, confirmFlag } from "./flag";
import { showProfile, showUsers, userHighScore } from "./users";
import { startRender } from "./render";
import { login, lock } from "./login";

interface token {
  id: number;
  role: string;
  user: string;
  points: number;
}

export type TokenData = { [key: string]: token };

export type Role = "admin" | "member";

async function main() {
  const token_storage: TokenData = {};
  const conf = await fs.readFile("./config.json", "utf-8").then(JSON.parse);

  const db = await mysql.createConnection({
    host: conf.host,
    user: conf.user,
    password: conf.password,
    database: conf.database,
  });

  const render = await startRender(
    "high-score.html",
    "users.html",
    "flags.html",
    "flag.html",
    "profile.html",
    "not-allowed.html",
    "not-logged-in.html",
    "logged-in.html",
    "wrong-password.html"
  );

  const app = express();

  app.use(logger);

  app.use(express.urlencoded({ extended: true }));

  app.use(cookieParser());

  // For testing
  app.get("/error", function (req, res) {
    throw "Test throwing Error";
  });

  // For all visitors
  app.use("/user", lock(token_storage, ["admin", "member"], render));
  app.use("/", express.static("public"));
  app.get("/login", login(token_storage, db, render));
  app.get("/flags", showFlags(render, db));
  app.get("/flag/:alias", showFlag(render, db));

  // For all users
  app.use("/profile", lock(token_storage, ["admin", "member"], render));
  app.get("/profile", showProfile(render, db));
  app.use("/capture", lock(token_storage, ["admin", "member"], render));
  app.get("/capture", confirmFlag(token_storage, db));
  //app.use("/flags", lock(token_storage, "all"));
  app.get("/flags", showFlags(render, db));
  //app.use("/garden", lock(token_storage, "all"));
  //app.get("/garden", showFlags(render, db));
  //app.use("/high-score", lock(token_storage, "admin"));
  //app.get("/high-score", userHighScore(render, db));
  app.use("/users", lock(token_storage, ["admin"], render));
  app.get("/users", showUsers(render, db));

  // For errors
  app.use(serverError);
  app.use(notFound);

  const port = conf.port;

  app.listen(port, () => {
    console.log("You can find this server on: http://localhost:" + port);
  });
}

main();

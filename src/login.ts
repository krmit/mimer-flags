import { NextFunction } from "express";
import { Request, Response } from "express";
import mysql from "mysql2/promise";
import crypto from "crypto";
import { v4 as uuid } from "uuid";
import { TokenData, Role } from "./main";

/* Midelware for locking sites */
export function lock(
  storage: TokenData,
  role: Role[],
  render: (path: string, data: any) => string
) {
  return function (req: Request, res: Response, next: NextFunction) {
    let token = req.cookies.login;
    let user = storage[token];
    if (user) {
      if (role.indexOf(user.role as Role) !== -1) {
        res.locals.user = user;
        next();
      } else {
        res.status(403).send(render("not-allowed.html", { user: user.user }));
        console.error("Not allowed by user: " + user);
      }
    } else {
      res.status(401).send(render("not-logged-in.html", {}));
      console.error("Not logged in");
    }
  };
}

export function login(
  storage: TokenData,
  db: mysql.Connection,
  render: (path: string, data: any) => string
) {
  return async function (req: Request, res: Response) {
    const user_name = req.query.user;
    if (
      req.query &&
      typeof req.query.user === "string" &&
      typeof req.query.password === "string"
    ) {
      const user_alias = req.query.user;
      const password = crypto
        .createHash("md5")
        .update(req.query.password)
        .digest("hex");
      let question =
        'SELECT id, alias,password, role FROM user WHERE alias ="' +
        user_alias +
        '"';
      const msg_promise = await db.execute(question);
      const answer = msg_promise.values().next().value[0];
      const password_from_database = answer.password;
      const role = answer.role;
      const id = Number(answer.id);
      if (password_from_database === password) {
        const token = uuid();
        question =
          "SELECT SUM(points) FROM flag WHERE id IN (SELECT flag FROM result WHERE user='" +
          id +
          "');";
        const sum_data = await db.execute(question);
        const sum = Number(
          sum_data.values().next().value[0]["SUM(points)"] ?? 0
        );
        storage[token] = {
          id: id,
          role: role,
          user: user_alias,
          points: sum,
        };
        res.cookie("login", token, { maxAge: 10000000 });
        res.status(200).send(render("logged-in.html", storage[token]));
        console.log("You are logged in as " + user_alias + "!");
      } else {
        res
          .status(403)
          .send(render("wrong-password.html", { user: user_alias }));
        console.log("Wrong password for " + user_alias + "!");
      }
    } else {
      res.status(403).send(render("no-password-user.html", {}));
      console.log("No passwor or user info!");
    }
  };
}

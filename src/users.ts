import { Request, Response } from "express";
import mysql from "mysql2/promise";

export function showUsers(
  render: (path: string, data: any) => string,
  db: mysql.Connection
) {
  return async function (req: Request, res: Response) {
    db.execute("SELECT alias, role FROM user;").then((user) => {
      let data = {
        user: user[0],
      };
      let result = render("users.html", data);
      res.send(result);
    });
  };
}

export function showProfile(
  render: (path: string, data: any) => string,
  db: mysql.Connection
) {
  return async function (req: Request, res: Response) {
    db.execute(
      "SELECT flag.alias, flag.title, flag.points, result.created FROM flag, result WHERE result.user=+" +
        res.locals.user.id +
        " AND result.flag=flag.id;"
    ).then((flags) => {
      let data = Object.assign({ flags: flags[0] }, res.locals.user);
      console.log(data.flags);
      let result = render("profile.html", data);
      res.send(result);
    });
  };
}

export function userHighScore(
  render: (path: string, data: any) => string,
  db: mysql.Connection
) {
  return async function (req: Request, res: Response) {
    let keywords_data = await db.execute("SELECT alias, role FROM user;");
    let keywords = keywords_data.values().next().value;
    const data = {
      keywords: keywords,
    };
    let result = render("high-score.html", data);
    res.send(result);
  };
}
